package org.mssa.exo15;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;


public class PersonWriter {

	public static void writer(List<Person> people, String filename) throws IOException {

		BufferedWriter bw = new BufferedWriter(new FileWriter(filename, true));
		try {
			for (Person p : people) {
				bw.newLine();
				bw.append(p.getLastName() + ", " + p.getFirstName() + ", " + Integer.toString(p.getAge()));
			}
			bw.flush();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (bw != null)
				bw.close();
			
		}
	}
}

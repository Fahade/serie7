package org.mssa.exo15;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Stream;

public class PersonReader {

	public static List<Person> read(String filename) throws IOException {
		List<Person> personnes = new ArrayList<Person>();
		BufferedReader br = new BufferedReader(new FileReader(filename));
		Function<String,Person> f = s->  new Person(s.split(", ")[0],s.split(", ")[1],Integer.parseInt(s.split(", ")[2]));
		Stream<String> str = br.lines();
		str.filter(s->s.charAt(0)!='#').forEach(s->personnes.add(f.apply(s)));
		br.close();
		return personnes;
	}
}

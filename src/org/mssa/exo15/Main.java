package org.mssa.exo15;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;


public class Main {
	
	
	@SuppressWarnings("resource")
	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		String filename = ("files/Personnes.txt");
		//Q1 : Stream implemente AutoCloseable 
		
		//Q2 
		File file = new File(filename);
		BufferedReader br = new BufferedReader(new FileReader(file));
		String line = br.readLine();
		line = br.readLine();
		
		Function<String,Person> f = s->  new Person(s.split(", ")[0],s.split(", ")[1],Integer.parseInt(s.split(", ")[2]));
		System.out.println(f.apply(line).getLastName() +", "+ f.apply(line).getFirstName() + ", " + f.apply(line).getAge());
		
		List<Person> personnes = PersonReader.read(filename);
		for(Person p : personnes) {
			System.out.println(p.getLastName() + ", " + p.getFirstName() + ", " + p.getAge());
		}
		
		//Q3
		Person p1 = new Person("Jordan","Micheal",50);
		Person p2 = new Person("Curry","Steph",27);
		Person p3 = new Person("Harden","James",50);

		
		List<Person> personnes2 = new ArrayList<Person>();
		personnes2.add(p1);
		personnes2.add(p2);
		personnes2.add(p3);
		
		PersonWriter.writer(personnes2, filename);

	}

}
